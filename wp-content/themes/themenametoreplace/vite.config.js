// https://vitejs.dev/config/
// http://localhost:3005 is serving Vite on development but accessing it directly will be empty

import { defineConfig } from 'vite'

const { resolve } = require('path')

export default defineConfig({
  resolve: {
    alias: {
      '@images': resolve(__dirname, './images'),
    },
  },
  build: {
    // output dir for production build
    outDir: resolve(__dirname, './build'),
    emptyOutDir: true,
    manifest: false,
    target: 'modules',
    // our entry
    rollupOptions: {
      input: resolve(__dirname, './main.js'),
      output: {
        manualChunks: undefined,
        assetFileNames: '[name].[ext]',
        entryFileNames: '[name].js',
      },
    },
  },
})
