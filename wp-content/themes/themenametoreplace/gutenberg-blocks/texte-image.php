<?php

/**
 * Texte image Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

?>


<section class="gut-block gut-texte-image" <?php if ( !empty($block['anchor']) ): ?>data-anchor="<?php echo $block['anchor']; ?>"<?php endif; ?>>
    <div class="wrap">
        <?php if ( get_field('image') ): ?>
            <div class="img-part">
                <div class="img">
                    <?php echo wp_get_attachment_image( get_field('image'), 'banner' ); ?>
                </div>
            </div>
        <?php endif; ?>
        
        <div class="content-part">
            <?php if ( get_field('titre') ): ?>
                <h3 class="block-title bicolore"><?php the_field('titre'); ?></h3>
            <?php endif; ?>

            <?php if ( get_field('texte') ): ?>
                <div class="texte wysiwyg-content">
                    <?php the_field('texte'); ?>
                </div>
            <?php endif; ?>

            <?php if ( get_field('cta') ):
                $cta = get_field('cta');
                $cta_url = $cta['url'];
                $cta_title = $cta['title'];
                $cta_target = $cta['target'] ? $cta['target'] : '_self'; ?>
                <div class="btn-container">
                    <a href="<?php echo esc_url($cta_url); ?>" target="<?php echo esc_attr($cta_target); ?>" class="btn"><?php echo esc_html($cta_title); ?></a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>