<?php

/**
 * Theme name to replace functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Theme name to replace
 */

if (!function_exists('theme_name_to_replace_setup')) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function theme_name_to_replace_setup()
  {

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    /*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
    add_theme_support('title-tag');

    add_theme_support('custom-logo', array(
      'height'      => 240,
      'width'       => 240,
      'flex-height' => true,
      'flex-width'  => true,
    ));

    /*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(array(
      'primary' => 'Menu principal',
    ));

    /*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
    add_theme_support('html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ));
  }
endif;
add_action('after_setup_theme', 'theme_name_to_replace_setup');

if (!function_exists('twentysixteen_the_custom_logo')) :
  /**
   * Displays the optional custom logo.
   *
   * Does nothing if the custom logo is not available.
   */
  function theme_name_to_replace_the_custom_logo()
  {
    if (function_exists('the_custom_logo')) {
      the_custom_logo();
    }
  }
endif;

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function theme_name_to_replace_content_width()
{
  $GLOBALS['content_width'] = apply_filters('theme_name_to_replace_content_width', 640);
}
add_action('after_setup_theme', 'theme_name_to_replace_content_width', 0);

/**
 * Enqueue scripts and styles.
 */
function theme_name_to_replace_scripts()
{
  wp_enqueue_style('theme_name_to_replace-style', get_stylesheet_uri());

  // register stylesheet
  wp_register_style('theme', get_stylesheet_directory_uri() . '/build/main.css', array(), '', 'all');

  // if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
  // 	wp_enqueue_script( 'comment-reply' );
  // }

  // register script
  wp_register_script('theme', get_stylesheet_directory_uri() . '/build/main.js', array('jquery'), '', true);

  // enqueue styles and scripts
  wp_enqueue_style('theme');
  wp_enqueue_script('theme');
}
add_action('wp_enqueue_scripts', 'theme_name_to_replace_scripts');


// Désactivation des emails de notifications de mise à jour automatique
function theme_name_to_replace_stop_auto_update_emails($send, $type, $core_update, $result)
{
  if (!empty($type) && $type == 'success') {
    return false;
  }
  return true;
}

add_filter('auto_core_update_send_email', 'theme_name_to_replace_stop_auto_update_emails', 10, 4);
add_filter('auto_plugin_update_send_email', '__return_false');
add_filter('auto_theme_update_send_email', '__return_false');


// Suppression Emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');


// Images sizes
add_image_size('banner', 1920, 1080, false);


// CF7 no auto <p>
add_filter('wpcf7_autop_or_not', '__return_false');


// Page d'options
if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
    'page_title'   => 'Theme options',
    'menu_slug'   => 'theme-options',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
}

// Remove content editor for some pages
function theme_name_to_replace_remove_editor_init()
{
  if (!is_admin()) {
    return;
  }

  $current_post_id = filter_input(INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT);
  $update_post_id = filter_input(INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT);

  if (isset($current_post_id)) {
    $post_id = absint($current_post_id);
  } else if (isset($update_post_id)) {
    $post_id = absint($update_post_id);
  } else {
    return;
  }

  if (isset($post_id)) {
    $template_file = get_post_meta($post_id, '_wp_page_template', true);

    if (
      $post_id == get_option('page_on_front')
      || $post_id == get_option('page_for_posts')
      || $template_file === 'page-templates/contact.php'
    ) {
      remove_post_type_support('page', 'editor');
    }
  }
}
add_action('init', 'theme_name_to_replace_remove_editor_init');

// Gutenberg categories
function theme_name_to_replace_block_categories($categories)
{
  $theme_name_to_replace_category = array(
    'slug' => 'theme_name_to_replace',
    'title' => 'Theme name to replace',
  );

  array_unshift($categories, $theme_name_to_replace_category);

  return $categories;
}
add_filter('block_categories_all', 'theme_name_to_replace_block_categories', 10, 2);

// Gutenberg blocks
function theme_name_to_replace_register_acf_block_types()
{
  acf_register_block_type(array(
    'name'              => 'texte-image',
    'title'             => __('Texte et image'),
    'render_template'   => 'gutenberg-blocks/texte-image.php',
    'category'          => 'theme_name_to_replace',
    'icon'              => 'align-pull-right',
    'keywords'          => array('texte', 'image'),
    'mode'              => 'edit',
    'supports'          => array('anchor' => true),
  ));
}

if (function_exists('acf_register_block_type')) {
  add_action('acf/init', 'theme_name_to_replace_register_acf_block_types');
}

// Gutenberg allowed block types
add_filter('allowed_block_types_all', 'theme_name_to_replace_abt');

function theme_name_to_replace_abt($allowed_block_types)
{
  return array(
    'core/block',

    // Custom
    'acf/texte-image',

    // Common
    'core/paragraph',
    'core/image',
    'core/heading',
    'core/gallery',
    'core/list',
    'core/list-item',
    'core/quote',
    'core/audio',
    'core/cover',
    'core/file',
    // 'core/video',

    // Formatting
    // 'core/table',
    // 'core/verse',
    // 'core/code',
    // 'core/freeform — Classic',
    // 'core/html — Custom HTML',
    // 'core/preformatted',
    // 'core/pullquote',

    // Layout
    // 'core/buttons',
    // 'core/text-columns',
    // 'core/media-text',
    // 'core/more',
    // 'core/nextpage',
    // 'core/separator',
    // 'core/spacer',

    // Widgets
    // 'core/shortcode',
    // 'core/archives',
    // 'core/categories',
    // 'core/latest-comments',
    // 'core/latest-posts',
    // 'core/calendar',
    // 'core/rss',
    // 'core/search',
    // 'core/tag-cloud',

    // Embed
    // 'core/embed',
    // 'core-embed/twitter',
    // 'core-embed/youtube',
    // 'core-embed/facebook',
    // 'core-embed/instagram',
    // 'core-embed/wordpress',
    // 'core-embed/soundcloud',
    // 'core-embed/spotify',
    // 'core-embed/flickr',
    // 'core-embed/vimeo',
    // 'core-embed/animoto',
    // 'core-embed/cloudup',
    // 'core-embed/collegehumor',
    // 'core-embed/dailymotion',
    // 'core-embed/funnyordie',
    // 'core-embed/hulu',
    // 'core-embed/imgur',
    // 'core-embed/issuu',
    // 'core-embed/kickstarter',
    // 'core-embed/meetup-com',
    // 'core-embed/mixcloud',
    // 'core-embed/photobucket',
    // 'core-embed/polldaddy',
    // 'core-embed/reddit',
    // 'core-embed/reverbnation',
    // 'core-embed/screencast',
    // 'core-embed/scribd',
    // 'core-embed/slideshare',
    // 'core-embed/smugmug',
    // 'core-embed/speaker',
    // 'core-embed/ted',
    // 'core-embed/tumblr',
    // 'core-embed/videopress',
    // 'core-embed/wordpress-tv',

  );
}

// ACF blocks required fields
add_action('acf/validate_save_post', 'theme_name_to_replace_validate_save_post', 5);
function theme_name_to_replace_validate_save_post()
{
  $acf = false;
  foreach ($_POST as $key => $value) {
    if (strpos($key, 'acf') === 0) {
      if (!empty($_POST[$key])) {
        acf_validate_values($_POST[$key], $key);
      }
    }
  }
}

// Unbreakable spaces
function theme_name_to_replace_acf_format_value($value, $post_id, $field)
{
  $value = preg_replace('/\s(!|\?|:)/m', '&nbsp;$1', $value);
  return $value;
}
add_filter('acf/format_value/type=text', 'theme_name_to_replace_acf_format_value', 10, 3);
add_filter('acf/format_value/type=medium_editor', 'theme_name_to_replace_acf_format_value', 10, 3);
add_filter('acf/format_value/type=wysiwyg', 'theme_name_to_replace_acf_format_value', 10, 3);
