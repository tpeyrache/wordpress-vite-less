<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Theme name to replace
 */

?>

			<footer id="footer" role="contentinfo">
				<?php if ( have_rows('reseaux_sociaux', 'option') ): ?>
					<ul class="social">
						<?php while ( have_rows('reseaux_sociaux', 'option') ): the_row(); ?>
							<?php $reseau = get_sub_field('reseau'); ?>
							<li>
								<a href="<?php the_sub_field('url'); ?>" target="_blank" title="<?php echo $reseau['label']; ?>" aria-label="<?php echo $reseau['label']; ?>">
									<span class="icon icon-<?php echo $reseau['value']; ?>" aria-hidden="true"></span>
								</a>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>

				<?php wp_nav_menu(array(
					'container' => false,
					'menu_class' => 'footer-menu',
					'theme_location' => 'footer',
					'depth' => 0,
				)); ?>
			
			<p>test</p>
			</footer>
			
		</main><!-- #page -->

		<?php wp_footer(); ?>

	</body>
</html>
